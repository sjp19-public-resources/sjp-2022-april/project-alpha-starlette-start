import { projectsUrl, projectCreateUrl, mockProject, projectShowUrlRegex } from '../static';


describe('The Project Create Flow', () => {
    beforeEach(() => {
        cy.visit(projectCreateUrl);
    });

    it('has a create project page with a properly built form', () => {
        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form').should('have.attr', 'action', projectsUrl);
        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('textarea[name="description"]');
            cy.get('button[type="submit"]').contains('Create Project');
        });
    });

    it('submitting the form creates a new project and displays it on the show page', () => {
        cy.get('input[name="name"]').type(mockProject.name);
        cy.get('textarea[name="description"]').type(mockProject.description);
        cy.get('button[type="submit"]').click();

        cy.url().should('match', projectShowUrlRegex);

        cy.contains(`${mockProject.name}`);
        cy.contains(`${mockProject.description}`);
    });
});

describe('The project Create Flow effects the list page', () => {
    it('the new project also displays on the list page as a link', () => {
        cy.visit(projectsUrl);
        cy.contains('a', mockProject.name)
            .should('have.attr', 'href')
            .should('match', projectShowUrlRegex);
    });
});
