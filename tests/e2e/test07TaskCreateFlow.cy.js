const { ObjectId } = require("bson");

import { projectsUrl, mockProject, employeesUrl, mockEmployee1, taskCreateUrlRegex, projectShowUrlRegex, mockTask, tasksUrlRegex } from '../static';


let newEmployeeId = null;

describe('The Task Create Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: projectsUrl,
            body: mockProject,
        });
        cy.request({
            ...commonReqProps,
            url: employeesUrl,
            body: mockEmployee1,
        }).then(resp => {
            newEmployeeId = resp.headers.location.replace(`${employeesUrl}/`, '');
        });
    });

    beforeEach(() => {
        cy.visit(projectsUrl);
        cy.contains('a', `${mockProject.name}`).click();
        cy.contains('a', `${mockTask.name}`).should('not.exist');
    });

    it('has a create task button on the project display page', () => {
        cy.contains('a', 'Create a new task')
            .should('have.attr', 'href')
            .should('match', taskCreateUrlRegex);
    });

    it('has a create task page with a properly built form', () => {
        cy.contains('a', 'Create a new task').click();
        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form').should('have.attr', 'action').should('match', tasksUrlRegex);
        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('textarea[name="description"]');
            cy.get('input[name="start_date"]').should('have.attr', 'type', 'date');
            cy.get('input[name="due_date"]').should('have.attr', 'type', 'date');
            cy.get('select[name="state"]');
            cy.get('select[name="state"] option[value="backlog"]');
            cy.get('select[name="state"] option[value="to do"]');
            cy.get('select[name="state"] option[value="in progress"]');
            cy.get('select[name="state"] option[value="in review"]');
            cy.get('select[name="state"] option[value="done"]');
            cy.get('select[name="assignee_id"]');
            cy.get(`select[name="assignee_id"] option[value="${newEmployeeId}"]`);
            cy.get('button[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form creates a new employee and displays it on the show page', () => {
        cy.contains('a', 'Create a new task').click();

        cy.get('input[name="name"]').clear().type(mockTask.name);
        cy.get('textarea[name="description"]').clear().type(mockTask.description);
        cy.get('input[name="start_date"]').clear().type(mockTask.start_date);
        cy.get('input[name="due_date"]').clear().type(mockTask.due_date);
        cy.get('select[name="state"]').select(mockTask.state);
        cy.get('select[name="assignee_id"]').select(mockEmployee1.name);
        cy.get('button[type="submit"]').click();

        cy.url().should('match', projectShowUrlRegex);

        cy.contains(`${mockTask.name}`);
        cy.contains(`${mockEmployee1.name}`);
    });
});

describe('The Task Create Flow adds the task to the Employee display page', () => {
    it('displays the task on the employee\'s page', () => {
        cy.visit(employeesUrl);
        cy.get('tr td a').last().click();
        cy.contains(`${mockTask.name}`);
    });
});
