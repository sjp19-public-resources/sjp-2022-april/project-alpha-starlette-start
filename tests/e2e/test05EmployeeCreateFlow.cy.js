import { employeesUrl, employeeCreateUrl, mockEmployee1, employeeShowUrlRegex } from '../static';


describe('The Employee Create Flow', () => {
    beforeEach(() => {
        cy.visit(employeeCreateUrl);
    });

    it('has a create employee page with a properly built form', () => {
        cy.get('form').should('have.attr', 'method', 'POST');
        cy.get('form').should('have.attr', 'action', employeesUrl);
        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('input[name="title"]').should('have.attr', 'type', 'text');
            cy.get('button[type="submit"]').contains('Create');
        });
    });

    it('submitting the form creates a new employee and displays it on the show page', () => {
        cy.get('input[name="name"]').type(mockEmployee1.name);
        cy.get('input[name="title"]').type(mockEmployee1.title);
        cy.get('button[type="submit"]').click();

        cy.url().should('match', employeeShowUrlRegex);

        cy.contains(`${mockEmployee1.name}`);
        cy.contains(`${mockEmployee1.title}`);
    });
});

describe('The Employee Create Flow effects the list page', () => {
    it('the new employee also displays on the list page as a link', () => {
        cy.visit(employeesUrl);
        cy.contains('a', mockEmployee1.name)
            .should('have.attr', 'href')
            .should('match', employeeShowUrlRegex);
    });
});
