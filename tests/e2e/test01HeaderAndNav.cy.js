import { projectsUrl, projectCreateUrl, employeesUrl, employeeCreateUrl } from '../static';


describe('The shared (base.html) header and nav', () => {
    beforeEach(() => {
        cy.visit('/projects');
    });

    it('successfully loads', () => {
        cy.request('/projects').then((response) => {
            expect(response.status).to.eq(200);
        });
    });
    
    it('has an h1 for the app\'s name with class logo', () => {
        cy.get('header h1.logo').should('contain', 'Task Projector');
    });
    
    it('has a nav bar with link elements to projects list and create, and employees list and create', () => {
        cy.get('header nav');
        cy.get(`header nav a[href="${projectsUrl}"]`).contains('Projects');
        cy.get(`header nav a[href="${projectCreateUrl}"]`).contains('Create New Project');
        cy.get(`header nav a[href="${employeesUrl}"]`).contains('Employees');
        cy.get(`header nav a[href="${employeeCreateUrl}"]`).contains('Create New Employee');
    });

    it('nav bar Projects link works', () => {
        cy.get(`header nav a[href="${projectsUrl}"]`).click();
        cy.url().should('eq', Cypress.config().baseUrl + projectsUrl);
    });

    it('nav bar Create New Project link works', () => {
        cy.get(`header nav a[href="${projectCreateUrl}"]`).click();
        cy.url().should('eq', Cypress.config().baseUrl + projectCreateUrl);
    });

    it('nav bar Employees link works', () => {
        cy.get(`header nav a[href="${employeesUrl}"]`).click();
        cy.url().should('eq', Cypress.config().baseUrl + employeesUrl);
    });

    it('nav bar Create New Employee link works', () => {
        cy.get(`header nav a[href="${employeeCreateUrl}"]`).click();
        cy.url().should('eq', Cypress.config().baseUrl + employeeCreateUrl);
    });
});
