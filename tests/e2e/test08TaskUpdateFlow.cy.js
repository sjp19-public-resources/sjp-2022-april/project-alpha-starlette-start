import { ObjectId } from 'bson';

import { projectsUrl, mockEmployee1, mockEmployee2, taskShowUrlRegex, mockProject, mockTaskUpdated, taskUpdateUrlRegex, mockTask, projectShowUrlRegex, employeesUrl } from '../static';


let newEmployeeId1 = null;
let newEmployeeId2 = null;

describe('The Task Update Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: projectsUrl,
            body: mockProject,
        }).then(resp => {
            const projectId = resp.headers.location.replace(projectsUrl, '');
            cy.request({
                ...commonReqProps,
                url: employeesUrl,
                body: mockEmployee1,
            }).then(resp => {
                newEmployeeId1 = resp.headers.location.replace(`${employeesUrl}/`, '');
                cy.request({
                    ...commonReqProps,
                    url: `/${projectsUrl}${projectId}/tasks`,
                    body: { ...mockTask, assignee_id: newEmployeeId1 },
                });
            });
        });
        cy.request({
            ...commonReqProps,
            url: employeesUrl,
            body: mockEmployee2,
        }).then(resp => {
            newEmployeeId2 = resp.headers.location.replace(`${employeesUrl}/`, '');
        });
    });

    beforeEach(() => {
        cy.visit(projectsUrl);
        cy.get('tr td a').last().click();
        cy.contains('a', `${mockTaskUpdated.name}`).should('not.exist');
        cy.contains('a', `${mockTask.name}`).click();
    });

    it('has an update task page with a properly built form and shows the current task data', () => {
        cy.url().should('match', taskShowUrlRegex);

        cy.get('form')
            .should('have.attr', 'action')
            .should('match', taskUpdateUrlRegex);
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');

        cy.get('form').within(() => {
            cy.get('input[name="name"]')
                .should('have.attr', 'type', 'text')
                .should('have.attr', 'value')
                .should('eq', `${mockTask.name}`);
            cy.get('textarea[name="description"]')
                .invoke('val')
                .should('eq', `${mockTask.description}`);
            cy.get('input[name="start_date"]')
                .should('have.attr', 'type', 'date')
                .should('have.attr', 'value')
                .should('eq', `${mockTask.start_date}`);
            cy.get('input[name="due_date"]')
                .should('have.attr', 'type', 'date')
                .should('have.attr', 'value')
                .should('eq', `${mockTask.due_date}`);
            // below option is "backlog"
            cy.get('select[name="state"] option:selected')
                .should('have.attr', 'value')
                .should('eq', `${mockTask.state}`);
            cy.get('select[name="state"] option[value="to do"]');
            cy.get('select[name="state"] option[value="in progress"]');
            cy.get('select[name="state"] option[value="in review"]');
            cy.get('select[name="state"] option[value="done"]');
            cy.get('select[name="assignee_id"] option:selected')
                .should('have.attr', 'value')
                .should('eq', `${newEmployeeId1}`);
            cy.get(`select[name="assignee_id"] option[value="${newEmployeeId2}"]`);
            cy.get('button[type="submit"]').contains('Submit');
        });
    });

    it('submitting the form updates the task and displays it on the show page', () => {
        cy.get('input[name="name"]').clear().type(mockTaskUpdated.name);
        cy.get('textarea[name="description"]').clear().type(mockTaskUpdated.description);
        cy.get('input[name="start_date"]').clear().type(mockTaskUpdated.start_date);
        cy.get('input[name="due_date"]').clear().type(mockTaskUpdated.due_date);
        cy.get('select[name="state"]').select(mockTaskUpdated.state);
        cy.get('select[name="assignee_id"]').select(newEmployeeId2);
        cy.get('button[type="submit"]').click();

        cy.url().should('match', projectShowUrlRegex);

        cy.contains('tr td a', mockTaskUpdated.name);
        cy.contains('tr td', mockTaskUpdated.start_date);
        cy.contains('tr td', mockTaskUpdated.due_date);
        cy.contains('tr td', mockEmployee2.name);
        cy.contains('tr td', mockTaskUpdated.state);
    });
});

describe('The Task Update Flow effects the employee show page', () => {
    it('the updated task also displays on the employee\'s show page as a link', () => {
        cy.visit(`/employees/${newEmployeeId2}`);
        cy.contains('tr td', `${mockTaskUpdated.name}`);
    });
});
