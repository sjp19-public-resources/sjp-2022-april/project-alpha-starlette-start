import { employeesUrl, employeeDeleteUrlRegex, mockEmployee1 } from '../static';


describe('The Story Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: employeesUrl,
            body: mockEmployee1,
        });
    });

    beforeEach(() => {
        cy.visit(employeesUrl);
        cy.get('tr td a').last().as('lastLink', { type: 'static' });
        cy.get('tr td a').its('length').as('employeeLinksLength', { type: 'static' });
        cy.get('tr td a').last().click();
    });

    it('has a delete link on the show page', () => {
        cy.contains('a', 'Delete employee')
            .should('have.attr', 'href')
            .should('match', employeeDeleteUrlRegex);
    });

    it('clicking the "Delete employee" link removes it from the list stories page', () => {
        cy.contains('a', 'Delete employee').click();
        cy.url().should('eq', `${Cypress.config().baseUrl}${employeesUrl}`);
        cy.get('@lastLink').should('not.exist');
        cy.get('@employeeLinksLength').then(oldLength => {
            if (oldLength === 1) {
                cy.get('tr td a').should('not.exist');
            } else {
                cy.get('tr td a').its('length').should('eq' , oldLength - 1);
            }
        });
    });
});
