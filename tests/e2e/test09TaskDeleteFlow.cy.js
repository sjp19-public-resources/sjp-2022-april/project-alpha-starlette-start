import { ObjectId } from 'bson';

import { projectsUrl, mockEmployee1, mockProject, mockTask, employeesUrl, projectShowUrlRegex, taskDeleteUrlRegex } from '../static';


let newEmployeeId = null;

describe('The Task Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: projectsUrl,
            body: mockProject,
        }).then(resp => {
            const projectId = resp.headers.location.replace(projectsUrl, '');
            cy.request({
                ...commonReqProps,
                url: employeesUrl,
                body: mockEmployee1,
            }).then(resp => {
                newEmployeeId = resp.headers.location.replace(employeesUrl + '/', '');
                cy.request({
                    ...commonReqProps,
                    url: `/${projectsUrl}${projectId}/tasks`,
                    body: { ...mockTask, assignee_id: newEmployeeId },
                });
            });
        });
    });

    beforeEach(() => {
        cy.visit(projectsUrl);
        cy.get('tr td a').last().click();
        cy.contains('a', `${mockTask.name}`).as('taskLink', { type: 'static' });
        cy.contains('a', `${mockTask.name}`).click();
    });

    it('has a delete link on the task update form page', () => {
        cy.contains('a', "Delete this task")
            .should('have.attr', 'href')
            .should('match', taskDeleteUrlRegex);
    });

    it('clicking delete link removes it from the project show page', () => {
        cy.contains('a', "Delete this task").click();
        cy.url().should('match', projectShowUrlRegex);
        cy.get('@taskLink').should('not.exist');
    });
});

describe('Deleting the task updates the employee\'s show page', () => {
    it('The task no longer shows up on the employee\'s show page', () => {
        cy.visit(`/employees/${newEmployeeId}`);
        cy.contains('tr td', `${mockTask.name}`).should('not.exist');
    });
});
