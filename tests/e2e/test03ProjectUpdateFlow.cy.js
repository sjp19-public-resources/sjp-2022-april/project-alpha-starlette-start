import { projectsUrl, mockProjectUpdated, projectShowUrlRegex, projectUpdateUrlRegex, mockProject } from '../static';


describe('The Project Update Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: projectsUrl,
            body: mockProject,
        });
    });

    beforeEach(() => {
        cy.visit(projectsUrl);
        cy.contains('a', mockProjectUpdated.name).should('not.exist');
        cy.get('tr td a').last().click();
    });

    it('has an update link on the show page', () => {
        cy.contains('Update this project')
            .should('have.attr', 'href')
            .should('match', projectUpdateUrlRegex);
    });

    it('has a update project page with a properly built form', () => {
        cy.contains('Update this project').click();
        cy.url().should('match', projectUpdateUrlRegex);

        cy.get('form')
            .should('have.attr', 'action')
            .should('match', projectShowUrlRegex);
        cy.get('form')
            .should('have.attr', 'method')
            .should('eq', 'POST');

        cy.get('form').within(() => {
            cy.get('input[name="name"]').should('have.attr', 'type', 'text');
            cy.get('textarea[name="description"]');
            cy.get('button[type="submit"]').contains('Update Project');
        });
    });

    it('submitting the form updates the project and displays it on the show page', () => {
        cy.contains('Update this project').click();

        cy.get('input[name="name"]').clear().type(mockProjectUpdated.name);
        cy.get('textarea[name="description"]').clear().type(mockProjectUpdated.description);
        cy.get('button[type="submit"]').click();

        cy.url().should('match', projectShowUrlRegex);

        cy.contains(`${mockProjectUpdated.name}`);
        cy.contains(`${mockProjectUpdated.description}`);
    });
});

describe('The project Update Flow effects the list page', () => {
    it('the updated project also displays on the list page as a link', () => {
        cy.visit(projectsUrl);
        cy.contains('a', mockProjectUpdated.name)
            .should('have.attr', 'href')
            .should('match', projectShowUrlRegex);
    });
});
