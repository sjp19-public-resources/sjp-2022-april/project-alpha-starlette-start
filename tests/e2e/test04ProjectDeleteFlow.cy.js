import { projectsUrl, projectDeleteUrlRegex, mockProject } from '../static';


describe('The Story Delete Flow', () => {
    before(() => {
        const commonReqProps = { method: 'POST', form: true, followRedirect: false };
        cy.request({
            ...commonReqProps,
            url: projectsUrl,
            body: mockProject,
        });
    });

    beforeEach(() => {
        cy.visit(projectsUrl);
        cy.get('tr td a').last().as('lastLink', { type: 'static' });
        cy.get('tr td a').its('length').as('projectLinksLength', { type: 'static' });
        cy.get('tr td a').last().click();
    });

    it('has a delete link on the show page', () => {
        cy.contains('a', 'Delete this project and all its tasks')
            .should('have.attr', 'href')
            .should('match', projectDeleteUrlRegex);
    });

    it('clicking the "Delete this project and ..." link removes it from the list stories page', () => {
        cy.contains('a', 'Delete this project and all its tasks').click();
        cy.url().should('eq', `${Cypress.config().baseUrl}${projectsUrl}`);
        cy.get('@lastLink').should('not.exist');
        cy.get('@projectLinksLength').then(oldLength => {
            if (oldLength === 1) {
                cy.get('tr td a').should('not.exist');
            } else {
                cy.get('tr td a').its('length').should('eq' , oldLength - 1);
            }
        });
    });
});
