const { v4: uuidv4 } = require('uuid');

const mockProject = {
    name: uuidv4(),
    description: "project description",
};

const mockProjectUpdated = {
    name: uuidv4(),
    description: "project description 2",
};

const mockEmployee1 = {
    name: uuidv4(),
    title: "instructor",
};

const mockEmployee2 = {
    name: uuidv4(),
    title: "instructor",
};

const mockTask = {
    name: uuidv4(),
    description: "some stuff needs doing",
    start_date: "2023-08-24",
    due_date: "2023-08-30",
    assignee_id: mockEmployee1._id,
    project_id: mockProject._id,
    state: "backlog",
};

const mockTaskUpdated = {
    name: uuidv4(),
    description: "newly updated todo",
    start_date: "2023-08-30",
    due_date: "2023-09-07",
    assignee_id: mockEmployee2._id,
    project_id: mockProjectUpdated._id,
    state: "in progress",
};

const projectsUrl = '/projects';
const projectCreateUrl = '/projects/create';
const projectShowUrlRegex = /projects\/(\d|[a-zA-Z]){24}/;
const projectUpdateUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/update/;
const projectDeleteUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/delete/;
const employeesUrl = '/employees';
const employeeCreateUrl = '/employees\/create';
const employeeShowUrlRegex = /employees\/(\d|[a-zA-Z]){24}/;
const employeeDeleteUrlRegex = /employees\/(\d|[a-zA-Z]){24}\/delete/;
const tasksUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/tasks/;
const taskCreateUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/tasks\/create/;
const taskShowUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/tasks\/(\d|[a-zA-Z]){24}/;
const taskUpdateUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/tasks\/(\d|[a-zA-Z]){24}\/update/;
const taskDeleteUrlRegex = /projects\/(\d|[a-zA-Z]){24}\/tasks\/(\d|[a-zA-Z]){24}\/delete/;

module.exports = {
    mockProject,
    mockProjectUpdated,
    mockEmployee1,
    mockEmployee2,
    mockTask,
    mockTaskUpdated,
    projectsUrl,
    projectCreateUrl,
    projectShowUrlRegex,
    projectUpdateUrlRegex,
    projectDeleteUrlRegex,
    employeesUrl,
    employeeCreateUrl,
    employeeShowUrlRegex,
    employeeDeleteUrlRegex,
    tasksUrlRegex,
    taskCreateUrlRegex,
    taskShowUrlRegex,
    taskUpdateUrlRegex,
    taskDeleteUrlRegex,
}
