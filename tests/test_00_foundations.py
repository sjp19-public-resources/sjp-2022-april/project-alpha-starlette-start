from bson import ObjectId
import os


def test_file_structure():
    assert os.path.isfile("./app.py"), "an app.py file exists in the root"
    assert os.path.isfile(
        "./templates/base.html"
    ), "a base.html template file exists in the templates dir"
    assert os.path.isfile(
        "./data_access/db_connection.py"
    ), "a db_connection.py file exists in the data_access dir"

    # projects
    assert os.path.isfile(
        "./controllers/projects.py"
    ), "a projects.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/projects.py"
    ), "a projects.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/projects.py"
    ), "a projects.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/projects.py"
    ), "a projects.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/projects/form.html"
    ), "a form template file exists in the templates/projects directory"
    assert os.path.isfile(
        "./templates/projects/list.html"
    ), "a list template file exists in the templates/projects directory"
    assert os.path.isfile(
        "./templates/projects/detail.html"
    ), "a detail template file exists in the templates/projects directory"

    # employees
    assert os.path.isfile(
        "./controllers/employees.py"
    ), "a employees.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/employees.py"
    ), "a employees.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/employees.py"
    ), "a employees.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/employees.py"
    ), "a employees.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/employees/create.html"
    ), "a create template file exists in the templates/employees directory"
    assert os.path.isfile(
        "./templates/employees/detail.html"
    ), "a detail template file exists in the templates/employees directory"
    assert os.path.isfile(
        "./templates/employees/list.html"
    ), "an list template file exists in the templates/employees directory"

    # tasks
    assert os.path.isfile(
        "./controllers/tasks.py"
    ), "a tasks.py file exists in the controllers directory"
    assert os.path.isfile(
        "./models/tasks.py"
    ), "a tasks.py file exists in the models directory"
    assert os.path.isfile(
        "./routes/tasks.py"
    ), "a tasks.py file exists in the routes directory"
    assert os.path.isfile(
        "./data_access/tasks.py"
    ), "a tasks.py file exists in the data_access dir"

    assert os.path.isfile(
        "./templates/tasks/form.html"
    ), "a form template file exists in the templates/tasks directory"


def test_root_imports():
    try:
        from data_access.db_connection import db

        assert (
            type(db).__name__ == "Database"
        ), "the db variable is a MongoClient's Database"
    except ImportError:
        assert False, "a db variable must exist in db_connection.py"

    try:
        from app import app

        assert (
            type(app).__name__ == "Starlette"
        ), "the app variable is an instance of a Starlette server"
    except ImportError:
        assert False, "an app variable must exist in app.py"


def test_project_imports():
    try:
        from routes.projects import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/projects.py"

    assert type(routes).__name__ == "list"

    try:
        from models.projects import Project
    except ImportError:
        assert False, "an Project class must exist in models/projects.py"


def test_project_model():
    from models.projects import Project

    project_data = {
        "name": "project name",
        "description": "project description",
        "extra": 1,
    }
    project = Project(**project_data)
    assert project.name == project_data["name"]
    assert project.description == project_data["description"]
    assert project._id is not None, "creates a default _id when none is given"
    try:
        assert project.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Project' object has no attribute 'extra'"
        ), "the Project class must ignore any extra parameters passed in to init"

    project_id = str(ObjectId())
    project = Project(**project_data, _id=project_id)
    assert project._id == project_id, "can also take in an _id"


def test_employee_imports():
    try:
        from routes.employees import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/employees.py"

    assert type(routes).__name__ == "list"

    try:
        from models.employees import Employee
    except ImportError:
        assert False, "a Employee class must exist in models/employees.py"


def test_employee_model():
    from models.employees import Employee

    employee_data = {
        "name": "james",
        "title": "instructor",
        "extra": 1,
    }
    employee = Employee(**employee_data)
    assert employee.name == employee_data["name"]
    assert employee.title == employee_data["title"]
    assert employee._id is not None, "creates a default _id when none is given"
    try:
        assert employee.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Employee' object has no attribute 'extra'"
        ), "the Employee class must ignore any extra parameters passed in to init"

    employee_id = str(ObjectId())
    employee = Employee(**employee_data, _id=employee_id)
    assert employee._id == employee_id, "can also take in an _id"


def test_task_imports():
    try:
        from routes.tasks import routes
    except ImportError:
        assert False, "a routes variable must exist in routes/tasks.py"

    assert type(routes).__name__ == "list"

    try:
        from models.tasks import Task
    except ImportError:
        assert False, "a Task class must exist in models/tasks.py"


def test_task_model():
    from models.tasks import Task

    task_data = {
        "name": "that thing todo",
        "description": "some stuff needs doing",
        "start_date": "2023-08-24",
        "due_date": "2023-08-30",
        "assignee_id": str(ObjectId()),
        "project_id": str(ObjectId()),
        "state": "backlog",
    }
    task = Task(**task_data)
    assert task.name == task_data["name"]
    assert task.description == task_data["description"]
    assert task.start_date == task_data["start_date"]
    assert task.due_date == task_data["due_date"]
    assert task.state == task_data["state"]
    assert task.assignee_id == task_data["assignee_id"]
    assert task.project_id == task_data["project_id"]
    assert task._id is not None, "creates a default _id when none is given"
    try:
        assert task.extra is None
    except AttributeError as ae:
        assert (
            str(ae) == "'Task' object has no attribute 'extra'"
        ), "the Task class must ignore any extra parameters passed in to init"

    task_id = str(ObjectId())
    task = Task(**task_data, _id=task_id)
    assert task._id == task_id, "can also take in an _id"
