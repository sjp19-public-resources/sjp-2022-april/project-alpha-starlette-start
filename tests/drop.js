const MongoClient = require("mongodb").MongoClient;


async function dropDB() {
    const client = new MongoClient(process.env.MONGODB_URL); //, { useNewUrlParser: true });

    try {
        await client.connect();
        console.log("Connected to server");

        const projectsCollection = client
            .db(process.env.DATABASE_NAME)
            .collection('projects');
        const employeesCollection = client
            .db(process.env.DATABASE_NAME)
            .collection('employees');

        await employeesCollection.drop();
        await projectsCollection.drop();
        console.log("Data dropped");
        client.close();
    } catch (err) {
        console.log(err.stack);
    }
}

dropDB();
