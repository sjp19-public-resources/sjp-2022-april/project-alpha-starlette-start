const MongoClient = require("mongodb").MongoClient;
const { ObjectId } = require("bson");
const {
    mockProject,
    mockProjectUpdated,
    mockEmployee1,
    mockEmployee2,
    mockTask,
    mockTaskUpdated,
} = require('./static');


async function seedDB() {
    const client = new MongoClient(process.env.MONGODB_URL);

    try {
        await client.connect();
        console.log("Connected to server");

        const employeesCollection = client
            .db(process.env.DATABASE_NAME)
            .collection('employees');
        const employee1 = { _id: `${new ObjectId()}`, ...mockEmployee1 };
        const employee2 = { _id: `${new ObjectId()}`, ...mockEmployee2 };
        const employeeData = [
            employee1,
            employee2,
        ];
        await employeesCollection.insertMany(employeeData);
        console.log("Employee data seeded");

        const projectsCollection = client
            .db(process.env.DATABASE_NAME)
            .collection('projects');
        const projectData = [
            { ...mockProject, _id: `${new ObjectId()}` },
            { ...mockProjectUpdated, _id: `${new ObjectId()}` },
        ];
        await projectsCollection.insertMany(projectData);
        console.log("Project data seeded");

        const tasksCollection = client
            .db(process.env.DATABASE_NAME)
            .collection('tasks');
        const tasksData = [
            {
                ...mockTask,
                _id: `${new ObjectId()}`,
                assignee_id: employee1._id,
                project_id: projectData[0]._id,
            },
            {
                ...mockTaskUpdated,
                _id: `${new ObjectId()}`,
                assignee_id: employee2._id,
                project_id: projectData[1]._id,
            }
        ]
        await tasksCollection.insertMany(tasksData);
        console.log("Task data seeded");

        client.close();
    } catch (err) {
        console.log(err.stack);
    }
}

seedDB();
