# Project Alpha with Starlette

[//]: <> (or provide your own description)
This webapp is designed to allow organization of projects
and subtasks. Manage your work well by creating tasks and
assigning them to teammembers as needed!

This project is built with Starlette, a python api framework.

## Getting started

1. create a python virtual environment
2. `pip install -r requirements.txt`
3. `npm install` (needed for automated browser tests written in js)
4. get started!! use each of the following resources (Learn,
    test output, documentation) in unison to complete your project
    - follow along with the instructions in Learn for context
    - use Test Driven Development (TDD) to see and fix the errors
        - `sh -ac '. ./.env; python -m pytest -s'`
        - once those foundational tests are passing you should be in a position
            to run the site with `python -m uvicorn --reload --env-file .env app:app`
            to see your existing UI
        - run the ui tests with `npm run test`
            - note: this one relies on a running server aka getting 3a to pass and step 6
            - these `npm` tests are dependent on data in your DB, for this we've provided:
                - for adding data: `npm run seed`
                - for clearing data: `npm run drop`
    - start with a stub by creating a Starlette app in an app.py file in the root of this project
        - follow the [starlette.io](https://www.starlette.io/) guide for an example
